import {RentService} from "../../model/service/rent.service";
import {Layout} from "../../components/layout";
import Table from 'react-bootstrap/Table';
import {Rent} from "../../model/entity/rent";
import {Renter} from "../../model/entity/renter";
import {Car} from "../../model/entity/car";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {solid} from "@fortawesome/fontawesome-svg-core/import.macro";
import * as fs from "fs";
import {useState} from "react";
import {ok} from "assert";
import {AlertComponent} from "../../components/alert/alertComponent";
import {TypeAlert} from "../../components/alert/EnumTypeAlert";

export const getServerSideProps = RentService.getServerSidePropsAllRents;

type RenterRent = {
    renter: Renter;
    cars: {
        car: Car,
        rent: Rent[];
    }[];
}


export default function Admin(post: any) {


    console.log(post.renters);

    const [renters, setRenters] = useState(post.renters);

    const [message, setMessage] = useState<string>("");
    const [title, setTitle] = useState<string>("");
    const [variant, setVariant] = useState<TypeAlert>(TypeAlert.danger);
    const [showAlert, setShowAlert] = useState<boolean>(false);

    const handleOnCloseAlert = () => {
        setShowAlert(false);
    }

    const handleOnDelete = async (rent: Rent) => {
        const res: Response = await RentService.deleteRent(rent);

        if (res.ok) {
            let r = renters;
            r.forEach((renter: any, i: number) => {
                renter.rents.forEach((re: any, j: number) => {
                    if (re === rent) {
                        r[i].rents.splice(j, 1);
                    }
                })
            })
            setRenters(r);
            setMessage(`La réservation du ${(new Date(rent.start)).toLocaleDateString()} au ${(new Date(rent.end)).toLocaleDateString()} a bien été supprimée`);
            setTitle("Réservation supprimée");
            setShowAlert(true);
        } else {
            const message: string = (await res.json()).message;
            setMessage(message);
            setTitle("Erreur");
            setShowAlert(true);
        }
    }

    return (
        <>
            <Layout path={"admin"}>
                <>
                    <AlertComponent
                        isTitled={true}
                        message={message}
                        onCloseAlert={handleOnCloseAlert}
                        show={showAlert}
                        typeAlert={variant}
                        title={title}
                    />
                    <div className="container mt-5">
                        <div className="row pt-5">
                            <div className="col"><h1 className="display-1">Administration</h1>
                                <p>Visualisation de toutes les locations</p>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid mt-5">
                        <div className="row">
                            <div className="col">
                                <Table borderless hover>
                                    <thead>
                                    <tr>
                                        <th><h5>Loueur</h5></th>
                                        <th><h5>Voiture</h5></th>
                                        <th><h5>Réservations</h5></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {renters.map((renter: any, i: number) => (
                                            <>
                                                {(renter.rents.length !== 0) ?
                                                    (<tr key={i} className={"shadow-sm"}>
                                                        <td>
                                                            <p><b>{renter.firstName}</b> <br/> <small>{renter.email}</small>
                                                            </p>
                                                            <p></p>
                                                        </td>
                                                        <td>
                                                        {renter.rents.map((rent: any, j: number) => (
                                                            <>
                                                                <p key={`${i}-${j}`}>{rent.car.brand}</p>
                                                            </>))}
                                                        </td>
                                                        <td>
                                                            {renter.rents.map((rent: any, j: number) => (
                                                                <>
                                                                    <p key={`${j}-${i}`}>
                                                                        {(new Date(rent.start)).toLocaleDateString()} - {(new Date(rent.end)).toLocaleDateString()}
                                                                        <Button type={"button"} size={"sm"}
                                                                                variant={"danger"} className={"mx-3"}
                                                                                onClick={() => {
                                                                                    handleOnDelete(rent)
                                                                                }}
                                                                        >Supprimer</Button>
                                                                    </p>
                                                                </>
                                                            ))}
                                                        </td>
                                                    </tr>) :
                                                    ("")}

                                            </>
                                        )
                                    )}
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div>
                </>
            </Layout>
        </>
    )

}