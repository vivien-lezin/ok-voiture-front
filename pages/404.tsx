import {Layout} from "../components/layout";
import Head from "next/head";
import utilStyles from "../styles/utils.module.scss";

export let siteTitle = "Erreur 404";
export let siteDescription = "La page demandée n'a pas été trouvé";

export default function Home() {

    return (
        <Layout path={"404"}>
            <>
                <div className="container mt-5">
                    <div className="row pt-5">
                        <div className="col">
                            <p>404</p>
                            <h1>Not found</h1>
                        </div>
                    </div>
                </div>
            </>
        </Layout>
    )
}