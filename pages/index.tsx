import {Layout} from '../components/layout';
import {CarService} from "../model/service/car.service";
import {Car} from "../model/entity/car";
import {CardCar} from "../components/car/card-car/card-car";
import React, {useState} from "react";
import {ModalRent} from "../components/modal/rent_car/modal";
import {TypeAlert} from "../components/alert/EnumTypeAlert";


type Props = {
    defaultCar: Car;
    cars: Car[];
}

type State = {
    cars: Car[];
    car: Car;
    showModal: boolean;
}



export const getServerSideProps = CarService.getServerSidePropsAllCars;

export default class CarList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        //Sort ASC the cars by price
        let cars: Car[] = this.props.cars.sort((a: Car, b: Car) => {
            if (a.priceByDay < b.priceByDay) return -1;
            if (a.priceByDay > b.priceByDay) return 1;
            return 0;
        });


        this.state = {
            cars: cars,
            car: this.props.defaultCar,
            showModal: false
        };
        this.setCarFromCardCar = this.setCarFromCardCar.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

    }


    private readonly setCarFromCardCar = (c:Car)=>{
        this.setState({
            car: c,
            showModal: true
        });
    }
    private readonly handleCloseModal = () => {
        this.setState({showModal:false})
    }

    render() {
        return (
            <>
                <Layout path="home">
                    <>

                        <div className="container pt-5">
                            <div className="row my-4">
                                <div className="col">
                                    <h1 className={"display-1"}>OkVoiture</h1>
                                    <p>OkVoiture est une application web de location de voiture entre particulier</p>
                                </div>
                            </div>
                            <div className="row g-3">
                                {this.state.cars.map((car: Car, index: number) => (
                                    <div key={index} className="col-12 col-lg-3 col-md-4 col-sm-6">
                                        <CardCar car={car} onShowModal={this.setCarFromCardCar}/>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </>
                </Layout>
                <ModalRent car={this.state.car} show={this.state.showModal} handleClose={this.handleCloseModal}/>
            </>
        )
    }

}