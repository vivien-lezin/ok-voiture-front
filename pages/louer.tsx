import React, {useState} from "react";
import {Layout} from "../components/layout";
import {Form, InputGroup} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {CarService} from "../model/service/car.service";
import {AlertComponent} from "../components/alert/alertComponent";
import {TypeAlert} from "../components/alert/EnumTypeAlert";


export const getStaticProps = CarService.getStaticPropsAllCities;

export default function Louer(post: any) {


    const [name, setName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [brand, setBrand] = useState<string>("");
    const [model, setModel] = useState<string>("");
    const [year, setYear] = useState<string>("");
    const [city, setCity] = useState<string>("");
    const [price, setPrice] = useState<string>("");
    const [photo, setPhoto] = useState<string>("");

    const [message, setMessage] = useState<string>("");
    const [title, setTitle] = useState<string>("");
    const [variant, setVariant] = useState<TypeAlert>(TypeAlert.danger);
    const [showAlert, setShowAlert] = useState<boolean>(false);

    const handleCloseAlert = () => {
        setShowAlert(false);
    }

    const handleSubmit = () => {
        const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailFormat)) {
            setShowAlert(true);
            setTitle("Erreur")
            setMessage("L'email rentrée n'est pas correcte");
            setVariant(TypeAlert.danger);
            return;
        }

        if (brand === "" || model === "" || year === "" || price === "" || city === "" || email === "" || name === "")
        {
            setShowAlert(true);
            setTitle("Erreur")
            setMessage("Il manque des champs requis");
            setVariant(TypeAlert.danger);
            return;
        }

        CarService.createCar({
            brand: brand,
            model: model,
            year: new Date(year),
            photo: photo?photo:undefined,
            priceByDay: parseInt(price),
            city: city,
            id:null,
            creationCar: null,
            renter: {
                email: email,
                firstName: name,
                createdAt:null
            }
        }).then(async r => {
            if (r.ok) {
                setShowAlert(true);
                setTitle("Voiture enregistrée")
                setMessage("La voiture a été enregistrée et est prête à être louée");
                setVariant(TypeAlert.success);
            } else {
                setShowAlert(true);
                setTitle("Erreur")
                setMessage((await r.json()).message);
                setVariant(TypeAlert.danger);
            }
        })
    }

    return (
        <>
            <Layout path={"louer"}>
                <>
                    <AlertComponent
                        isTitled={true}
                        message={message}
                        onCloseAlert={handleCloseAlert}
                        typeAlert={variant}
                        title={title}
                        show={showAlert}
                    />
                    <div className="container my-5">
                        <h1 className="display-1 pt-5">Proposer une voiture</h1>
                        <p>En tant que particulier, tu peux proposer ta voiture pour de la location et gagner de
                            l'argent.</p>
                        <Form noValidate>
                            <div className="row">
                                <div className="col-md mt-3">
                                    <Form.Group>
                                        <Form.Label>Ton prénom*</Form.Label>
                                        <Form.Control type={"text"} placeholder={"Entrer un prénom"} autoFocus
                                                      value={name}
                                                      onChange={(e) => {
                                                          setName(e.currentTarget.value);
                                                      }}
                                                      required/>
                                    </Form.Group>
                                </div>
                                <div className="col-md mt-3">
                                    <Form.Label>Ton mail*</Form.Label>
                                    <Form.Control type={"email"} placeholder={"Entrer un email"}
                                                  value={email}
                                                  onChange={(e) => {
                                                      setEmail(e.currentTarget.value);
                                                  }}
                                                  required/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg mt-3">
                                    <Form.Label>La marque de ta voiture*</Form.Label>
                                    <Form.Control type={"text"} placeholder={"Entrer une marque"}
                                                  value={brand}
                                                  onChange={(e) => {
                                                      setBrand(e.currentTarget.value);
                                                  }}
                                                  required/>
                                </div>
                                <div className="col-lg mt-3">
                                    <Form.Label>La model de ta voiture*</Form.Label>
                                    <Form.Control type={"text"} placeholder={"Entrer un model"}
                                                  value={model}
                                                  onChange={(e) => {
                                                      setModel(e.currentTarget.value);
                                                  }}
                                                  required/>
                                </div>
                                <div className="col-lg-3 mt-3">
                                    <Form.Label>L'année de ta voiture*</Form.Label>
                                    <Form.Control type={"date"} placeholder={"Entrer une année"}
                                                  value={year}
                                                  onChange={(e) => {
                                                      setYear(e.currentTarget.value);
                                                  }}
                                                  required/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg mt-3">
                                    <Form.Label>La ville de ta voiture*</Form.Label>
                                    <Form.Select placeholder={"Choisir une ville"}
                                                 value={city}
                                                 onChange={(e) => {
                                                     setCity(e.currentTarget.value);
                                                 }}
                                                 required>
                                        {/*Faire la boucle sur l'API*/}
                                        <option selected>--Sélectionner une ville--</option>
                                        {post.cities.map((nameCity: string, index: number) => (
                                            <option key={index} value={nameCity}>{nameCity}</option>
                                        ))}


                                    </Form.Select>
                                </div>
                                <div className="col-lg mt-3">
                                    <Form.Label>Le prix journalier à la location*</Form.Label>
                                    <InputGroup>
                                        <Form.Control required
                                                      type="number"
                                                      placeholder="Entrer un prix"
                                                      value={price}
                                                      onChange={(e) => {
                                                          setPrice(e.currentTarget.value);
                                                      }}/>
                                        <InputGroup.Text>€</InputGroup.Text>
                                    </InputGroup>
                                </div>
                                <div className="col-lg mt-3">
                                    <Form.Label>Une photo de ta voiture</Form.Label>
                                    <Form.Control type={"text"} placeholder={"Entrer l'url de ta photo"}
                                                  value={photo}
                                                  onChange={(e) => {
                                                      setPhoto(e.currentTarget.value);
                                                  }}/>
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col"><p><small>* Champs requis</small></p></div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <Button
                                        type={"button"}
                                        variant={"success"}
                                        onClick={() => {
                                            handleSubmit();
                                        }}
                                    >
                                        Enregistrer
                                    </Button>
                                </div>
                            </div>
                        </Form>

                    </div>
                </>

            </Layout>
        </>
    );
}