# Getting Started

Il est nécessaire d'avoir lancé au préalable le serveur.

Lancement de l'application :

```bash
npm run dev
# or
yarn dev
```

Ouvrir [http://localhost:3000](http://localhost:3000) avec le navigateur pour voir le résultat.


