import React from "react";
import Alert from 'react-bootstrap/Alert';
import {TypeAlert} from "./EnumTypeAlert";


type PropsAlert = {
    title: string;
    isTitled: boolean;
    message: string;
    typeAlert: TypeAlert;
    show: boolean;
    onCloseAlert: Function;
}

type State = {}

export class AlertComponent extends React.Component<PropsAlert, State> {
    constructor(props: PropsAlert) {
        super(props);
    }

    render() {
        return (
            <>
                {this.props.show?
                    <div className="container fixed-top mt-5">
                        <div className="row">
                            <div className="col-lg"></div>
                            <div className="col">
                                <Alert variant={this.props.typeAlert} onClose={() => this.props.onCloseAlert()}
                                       dismissible>
                                    {(this.props.isTitled) ?
                                        (<>
                                            <Alert.Heading>{this.props.title}</Alert.Heading>
                                            <p>
                                                {this.props.message}
                                            </p>
                                        </>) : (this.props.message)}
                                </Alert>
                            </div>
                            <div className="col-lg"></div>
                        </div>
                    </div>:""
                }

            </>
        );
    }
}