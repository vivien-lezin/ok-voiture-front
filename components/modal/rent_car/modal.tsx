import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import {Car} from "../../../model/entity/car";
import React from "react";
import {Form, Image, InputGroup} from "react-bootstrap";
import {RentService} from "../../../model/service/rent.service";
import {Renter} from "../../../model/entity/renter";
import {RenterService} from "../../../model/service/renter.service";
import {TypeAlert} from "../../alert/EnumTypeAlert";
import {AlertComponent} from "../../alert/alertComponent";

type Props = {
    show: boolean;
    car: Car;
    handleClose: Function;
}

type State = {
    validated: boolean;

    startDateValue: string;

    endDateValue: string;
    dateMessage: string;

    emailValue: string;
    emailMessage: string;

    nameValue: string;
    nameMessage: string;

    errorMessage: string;
    errorDisplay: boolean;

    alertMessage: string,
    alertTitle: string,
    alertDisplay: boolean,
}

export class ModalRent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            validated: false,

            endDateValue: "",

            startDateValue: "",
            dateMessage: "",

            emailValue: "",
            emailMessage: "",

            nameValue: "",
            nameMessage: "",

            errorMessage: "",
            errorDisplay: false,

            alertMessage: "",
            alertTitle: "",
            alertDisplay: false,

        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private async handleSubmit(event: any) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        const email = this.state.emailValue;
        const name = this.state.nameValue;


        let emailMessage: string = "";
        let nameMessage: string = "";
        let dateMessage: string = "";
        let error: boolean = false;

        if (name === "") {
            error = true;
            console.log("name requis : " + this.state.nameValue);
            nameMessage += "Tu ne pourras pas louer une voiture sans écrire ton prénom. ";
        }
        if (email === "") {
            error = true;
            console.log("email requis : " + this.state.emailValue);
            emailMessage += "Tu ne pourras pas louer de voiture sans ton adresse mail. ";
        }
        let mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!email.match(mailFormat)) {
            error = true;
            console.log("email format : " + this.state.emailValue);
            emailMessage += "L'email rentré n'est pas correct, il doit être au format exemple@exemple.ex. ";
        }
        if (this.state.startDateValue === "") {
            error = true;
            console.log("date de début : " + this.state.startDateValue);
            dateMessage += "La date de début doit être renseignée. ";
        }
        if (this.state.endDateValue === "") {
            error = true;
            console.log("date de fin : " + this.state.endDateValue);
            dateMessage += "La date de fin doit être renseignée. ";
        }
        const startDate = new Date(this.state.startDateValue);
        const endDate = new Date(this.state.endDateValue);


        if (!error) {
            let renter: Renter | null = await RenterService.getOneRenter(email);
            if (!renter) {
                await RenterService.createRenter({
                    email: email,
                    firstName: name,
                    createdAt: null
                }).then(res => {
                    if (res.status !== 201) {
                        //Affichage message d'erreur
                        this.setState({
                            errorDisplay: true,
                            errorMessage: "Problème lors de la réservation, essayes plus tard si le serveur répond mieux",
                        });
                        return;
                    }
                });
            }
            let rent: any = {
                start: startDate,
                end: endDate,
                car: this.props.car,
                renter: {
                    email: email,
                    firstName: name,
                    createdAt: null
                }
            };
            //Faire le post
            await RentService.createRent(rent).then(async (res) => {
                if (res.ok) {
                    //Message confirmation
                    this.setState({
                        alertDisplay: true,
                        alertTitle: "Location enregistrée",
                        alertMessage: "La location de la voiture a bien été enregistrée pour du " + startDate.toLocaleDateString("fr-fr", {
                            weekday: 'long',
                            year: 'numeric',
                            month: 'short',
                            day: 'numeric'
                        }) + " au " + endDate.toLocaleDateString("fr-fr", {
                            weekday: 'long',
                            year: 'numeric',
                            month: 'short',
                            day: 'numeric'
                        })
                    });
                    this.setState({
                        startDateValue: "",
                        endDateValue: "",
                        validated:false,
                        errorDisplay:false
                    })
                    this.props.handleClose();
                } else {

                    const message: string = (await res.json()).error;
                    //Affichage message d'erreur
                    this.setState({
                        errorDisplay: true,
                        errorMessage: message,
                    });
                    return;
                }
            })
        } else {
            this.setState({
                emailMessage: emailMessage,
                nameMessage: nameMessage,
                dateMessage: dateMessage,
                validated: true,
            });
        }

    }


    render() {

        return (
            <>
                <AlertComponent
                    show={this.state.alertDisplay}
                    isTitled={true}
                    message={this.state.alertMessage}
                    title={this.state.alertTitle}
                    typeAlert={TypeAlert.success}
                    onCloseAlert={() => {
                        this.setState({alertDisplay: false})
                    }}
                />

                <Modal show={this.props.show} onHide={() => this.props.handleClose()} size={"lg"}>
                    <Modal.Dialog className={"m-0"}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Location voiture
                            </Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-lg-4">
                                        <div className={"d-none d-lg-block"}>
                                            {(this.props.car.photo)?
                                                (<Image src={this.props.car.photo} className="d-block w-100"
                                                        alt={this.props.car.brand + " " + this.props.car.model}/>):
                                                (<Image src={"https://via.placeholder.com/640?text=Voiture"} className="d-block w-100" alt={this.props.car.brand + " " + this.props.car.model}/>)}

                                            <h5 className={"my-2"}>
                                                <b>{this.props.car.brand}</b> - {this.props.car.model}
                                            </h5>
                                            <div className="d-flex">
                                                <p className="h3">{this.props.car.priceByDay}</p>
                                                <p className="text-secondary"><small>€/jour</small></p>
                                            </div>
                                        </div>
                                        <div className="row mb-3 d-lg-none">
                                            <div className="col-4">
                                                {(this.props.car.photo)?
                                                    (<Image src={this.props.car.photo} className="d-block w-100"
                                                            alt={this.props.car.brand + " " + this.props.car.model}/>):
                                                    (<Image src={"https://via.placeholder.com/640?text=Voiture"} className="d-block w-100" alt={this.props.car.brand + " " + this.props.car.model}/>)}
                                            </div>
                                            <div className="col">
                                                <h5 className={"my-2"}>
                                                    <b>{this.props.car.brand}</b> - {this.props.car.model}</h5>
                                                <div className="d-flex">
                                                    <p className="h3">{this.props.car.priceByDay}</p>
                                                    <p className="text-secondary"><small>€/jour</small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg">
                                        <Form validated={this.state.validated}
                                              noValidate id={"formRent"}>
                                            <Form.Group controlId={"prenom"}>
                                                <Form.Label>Ton prénom*</Form.Label>
                                                <Form.Control type={"text"} placeholder={"Entrer un prénom"} autoFocus
                                                              value={this.state.nameValue}
                                                              onChange={(e) => {
                                                                  this.setState({nameValue: e.currentTarget.value})
                                                              }}
                                                              required/>
                                                <Form.Control.Feedback type="invalid">
                                                    {this.state.nameMessage}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group controlId={"email"} className={"mt-3"}>
                                                <Form.Label>Ton adresse mail*</Form.Label>
                                                <Form.Control type="email" placeholder="Entrer email"
                                                              value={this.state.emailValue}
                                                              required
                                                              onChange={(e) => {
                                                                  this.setState({emailValue: e.currentTarget.value})
                                                              }}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {this.state.emailMessage}
                                                </Form.Control.Feedback>
                                                <Form.Text className="text-muted">
                                                    <small>Nous ne communiquerons jamais ton email.</small>
                                                </Form.Text>
                                            </Form.Group>
                                            <Form.Label className={"mt-3"}>Quand souhaites-tu louer ?*</Form.Label>

                                            <InputGroup hasValidation>
                                                <InputGroup.Text>De</InputGroup.Text>
                                                <Form.Control required id={"startDate"}
                                                              type="date"
                                                              placeholder="Date de début"
                                                              value={this.state.startDateValue}
                                                              onChange={(e) => {
                                                                  this.setState({startDateValue: e.currentTarget.value})
                                                              }}/>
                                                <InputGroup.Text>à</InputGroup.Text>
                                                <Form.Control required id={"endDate"}
                                                              type="date"
                                                              placeholder="Date de fin"
                                                              value={this.state.endDateValue}
                                                              onChange={(e) => {
                                                                  this.setState({endDateValue: e.currentTarget.value})
                                                              }}/>
                                                <Form.Control.Feedback type="invalid">
                                                    {this.state.dateMessage}
                                                </Form.Control.Feedback>
                                            </InputGroup>
                                            <div className="mt-2">
                                                <Form.Text className="text-muted">
                                                    <small>* Champs requis</small>
                                                </Form.Text>
                                            </div>

                                        </Form>
                                        {(this.state.errorDisplay) ?
                                            <p className={"alert alert-danger my-3"}>{this.state.errorMessage}</p> : ""}

                                    </div>
                                </div>
                            </div>

                        </Modal.Body>

                        <Modal.Footer>

                            <Button variant="outline-danger" onClick={() => this.props.handleClose()}>Annuler</Button>
                            <Button type={"button"} form={"formRent"} variant="success"
                                    onClick={this.handleSubmit}>Louer</Button>
                        </Modal.Footer>

                    </Modal.Dialog>
                </Modal>
            </>

        );
    }
}