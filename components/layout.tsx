import Head from "next/head";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from "react-bootstrap/Button";
import {AlertComponent} from "./alert/alertComponent";
import React from "react";
import {TypeAlert} from "./alert/EnumTypeAlert";
import Link from "next/link";

export let siteTitle = 'Accueil - OkVoiture';
export let siteDescription = "Location de voiture";

type Props={
    path:string
    children : JSX.Element
}

export class Layout extends React.Component<Props>{

    render() {
        return (
            <>
                <Head>
                    <link rel="icon" href="/favicon.ico"/>
                    <meta
                        name="description"
                        content={siteDescription}
                    />
                    <meta
                        name="og:description"
                        content={siteDescription}
                    />
                    <title>{siteTitle}</title>
                    <meta name="og:title" content={siteTitle}/>
                </Head>
                <header className={"shadow-sm fixed-top"}>
                    <Navbar bg="light" expand="sm">
                        <Container>
                            <Navbar.Brand href="#home">OkVoiture</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="me-auto">
                                    {(this.props.path!=="home")?
                                        <Link href="/" className={"nav-link"}>Accueil</Link> : <div className={"nav-link active"}><b>Accueil</b></div>}
                                    <Link href="/louer" className={"btn btn-outline-primary"}>Proposer une voiture</Link>
                                </Nav>
                            </Navbar.Collapse>
                        </Container>
                    </Navbar>
                </header>
                <main className="my-5">{this.props.children}</main>
                <footer className="shadow-lg">
                    <div className="bg-light py-5">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <Link href={"/admin"}>Administration</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-dark py-1">
                        <div className="container">
                            <div className="row">
                                <div className="col"><small className="text-light"><i
                                    className="fa-solid fa-copyright"></i> Copyright - Vivien LEZIN</small></div>
                            </div>
                        </div>
                    </div>
                </footer>
            </>);
    }

}
