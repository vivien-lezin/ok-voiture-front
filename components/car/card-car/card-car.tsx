import {Image} from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import {Car} from "../../../model/entity/car";
import Link from "next/link";
import React, {PropsWithChildren, useEffect} from "react";

type Props ={
    car:Car;
    onShowModal: Function;
}

export class CardCar extends React.Component<Props> {

    private readonly car: Car;

    private setCarFromCardCar;

    constructor(props: any) {
        super(props);
        this.car = props.car;
        this.setCarFromCardCar = props.onShowModal
    }


    render() {
        return (
            <div className="card text-end shadow-sm border-0">
                {(this.car.photo)?
                    (<Image src={this.car.photo} className="card-img-top" alt={this.car.brand + " " + this.car.model}/>):
                    (<Image src={"https://via.placeholder.com/640?text=Voiture"} className="card-img-top" alt={this.car.brand + " " + this.car.model}/>)}

                <div className="card-body">
                    <h5 className="card-title"><b>{this.car.brand}</b> - {this.car.model}</h5>
                    <div className="card-text d-flex justify-content-end">
                        <p className="h3">{this.car.priceByDay}</p>
                        <p className="text-secondary"><small>€/jour</small></p>
                    </div>
                    <p><small>{this.car.city}</small></p>
                    <Button variant={"primary"} onClick={() => this.setCarFromCardCar(this.car)}>Réserver</Button>
                </div>
            </div>
        );
    }
}
