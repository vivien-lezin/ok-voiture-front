import {ConfigService} from "../config.service";
import {Renter} from "../entity/renter";
import {Rent} from "../entity/rent";

export class RenterService extends ConfigService{
    public static async getOneRenter(email: string): Promise<Renter | null> {
        try {
            const res = await fetch(this.URL_API + "/renter/" + email);
            return await res.json();

        } catch (e) {
            return null
        }

    }

    public static async getAllRenter() {
        return (await fetch(this.URL_API + "/renter")).json();
    }

    public static async createRenter(renter:Renter){
        return await this.post<Renter>("/renter", renter);
    }
}