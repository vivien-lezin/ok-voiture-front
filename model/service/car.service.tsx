import {Car} from "../entity/car";
import {GetStaticProps} from "next";
import {ConfigService} from "../config.service";

export class CarService extends ConfigService{
    public static async getAllCars():Promise<Car[]>{
        return (await fetch(this.URL_API+'/car')).json();
    }

    public static async getOneCar(id:string):Promise<Car>{
        return (await fetch(this.URL_API+"/car/"+id)).json();
    }

    public static async createCar(car:Car):Promise<Response>{
        return await super.post<Car>("/car", car);
    }

    public static getServerSidePropsAllCars: (context: any) => Promise<{ props: { defaultCar: Car; cars: Car[] } }> = async (
        context
    ) => {

        const cars: Car[] = await CarService.getAllCars();
        const defaultCar = cars[0];
        return {
            props: {
                defaultCar,
                cars
            },
        }
    }

    public static getStaticPropsAllCities:GetStaticProps<{ cities: string[] }> = async (
        context
    ) => {
        let cities: string[] = [];
        await fetch("https://geo.api.gouv.fr/departements/987/communes").then(async res => {
            await res.json().then(async response=>{
                await response.forEach((city: { nom: string; })=>{
                    console.log(city.nom);
                    cities.push(city.nom);
                });
            });
        });

        return {
            props: {
                cities
            },
        }
    }
}