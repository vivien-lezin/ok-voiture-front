import {Rent} from "../entity/rent";
import {ConfigService} from "../config.service";
import {GetStaticProps} from "next";

export class RentService extends ConfigService {

    public static async createRent(rent: Rent): Promise<Response> {
        return await super.post<Rent>("/rent", rent);
    }

    public static async getAllRent():Promise<Rent[]>{
        return (await fetch(this.URL_API+'/rent')).json();
    }

    public static async getAllRentByOrder():Promise<any>{
        return (await fetch(this.URL_API+'/rent/order')).json();
    }

    public static async deleteRent(rent: Rent): Promise<Response> {
        return await super.post<Rent>("/rent/delete", rent);
    }

    public static getServerSidePropsAllRents: GetStaticProps<{ renters: any }> = async (
        context
    ) => {
        return {
            props: {
                renters :await RentService.getAllRentByOrder()
            },
        }
    }
}