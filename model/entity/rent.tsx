import {Renter} from "./renter";
import {Car} from "./car";

export type Rent={
    id: number;

    start: Date;

    end: Date;

    renter:Renter;

    car:Car;
}