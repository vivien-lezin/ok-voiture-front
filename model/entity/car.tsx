import {Renter} from "./renter";


export type Car = {

    id: number|null;

    brand: string;

    model: string;

    year: Date;

    photo: string|undefined;

    priceByDay: number;

    city: string;

    creationCar:Date|null

    renter: Renter;
}