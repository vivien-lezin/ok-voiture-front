

export type Renter = {

    email:string;

    firstName: string;

    createdAt:Date|null
}