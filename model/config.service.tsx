export abstract class ConfigService {
    constructor() {
    }

    //TODO: A modifier lors de la mise en prod
    protected static readonly URL_API: string = "http://localhost:4000";


    protected static async post<T>(path: string, data: T) {
        return (await fetch(this.URL_API + path, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        }));


    }
}